# Manual do Desenvolvedor

Nesse manual tem como intuito a compreensão dos conceitos e arquitetura das tecnologias como protocolo AMQP, Servidor RabbitMQ e Middleware Orientado of Message (MOM) para que sirva de auxilio para a equipe de desenvolvimento.



## Conceitos do Protocolo AMQP

### *Exchanges* (intercambiador)

Pode-se dizer que o exchange possui o papel mais importante dentre os elementos do servidor AMQP. Sua tarefa é encaminhar as mensagens para as filas que correspondem às informações de roteamento, como por exemplo uma *routing key* (chave de roteamento), com os vínculos previamente definidos. Os *exchanges* (intercambiador) também podem rotear as mensagens de acordo com o seu tipo. Alguns destes tipos são:

*Direct* (Direto): *exchanges* (intercambiador) do tipo direto distribuem uma determinada mensagem quando a chave de roteamento houver uma correspondência exata com a *binding key* (chave de vínculo).

*Fanout*: *exchanges* (intercambiador) do tipo fanout distribuem as mensagens para todas as filas ligadas a eles, independente da sua chave de roteamento.

Tópico (Topic): *exchanges* (intercambiador) do tipo tópico direcionam as mensagens analisando a correspondência entre as chaves das mensagens e a dos vínculos das filas. 

### *Virtual Host*

Um Virtual Host é algo similar a um *workspace* (ambiente de trabalho) a qual o usuário se conecta. Ele inclui um conjunto de *Exchanges*, *message queues* e *bindings*. Mesmo que um usuário esteja autenticado no servidor, este pode ser barrado ao acessar um virtual host caso não seja autorizado. As autorizações para os virtual hosts são liberados para cada usuário do servidor.

### *Bindings* (Vínculos)

Um vínculo é uma relação entre uma fila e um exchange e define um critério para o direcionamento de mensagens.

### *Routing Key/Binding Key* (Chave de Roteamento/Chave de Vínculo)

As chaves de roteamento são os identificadores de mensagens. Do outro lado, as chaves de vínculos são os identificadores dos *bindings* (vínculos). *Binging key* (chaves de vínculo) existem para dizer aos exchanges de que as mensagens com identificadores que correspondem à ela sejam redirecionadas para a fila associada.

### *Message Queues* (Filas de Mensagem)

Filas de mensagem são as caixas de entrada de mensagens dos consumidores. Elas mantém as mensagens em memórias ou em disco até que elas sejam utilizadas pelos leitores. A fila implementa a política FIFO (*First In, First Out*).

## Protocolo AMQP

AMQP significa *Advanced Message Queuing Protocol* (Protocolo Avançado de Filas de Mensagem) e tem como seu principal propósito permitir a total interoperabilidade funcional entre clientes e servidores *middleware* de mensagens conhecidos também como *brokers* (corretor). Ou seja, AMQP define um protocolo padrão para publicação, enfileiramento, armazenamento e consumo de mensagens. O produtor de mensagens utiliza uma API para publicar as mensagens no servidor. O servidor/*broker* irá colocar as mensagens em filas e armazená-las enquanto estes não são consumidos. Por fim, uma aplicação irá consumir as mensagens removendo-as das filas.

O modelo do AMQP funciona da seguinte maneira: mensagens são publicadas pelos *publisher* (publicador) e  enviadas às exchanges. As exchanges distribuem as cópias das mensagens para filas utilizando regras que são chamadas de *bindings* (vínculos) no jargão do AMQP.

Então *brokers* (corretor) AMQP enviam mensagens para *subscribe* (consumidores) que acessam, ou assinam, as filas, ou consumidores buscam e processam mensagens da fila sob demanda.

Quando publicam uma mensagem, produtores podem especificar vários atributos para ela, também chamados de metadados da mensagem. Alguns destes atributos podem ser utilizados pelo *broker* (corretor), contudo, outros atributos são apenas utilizados pelas aplicações que recebem a mensagem.

Redes não são confiáveis e aplicações podem falhar em processar mensagens, portanto o modelo do AMQP tem a implementação de acknowledgments de mensagens, ou seja, quando uma mensagem é enviada a um *subscribre* (consumidor), o consumidor notifica o *broker* (corretor), seja automaticamente, seja desenvolvido posteriormente.

Quando acknowledgements estão sendo utilizados, um *broker* só removerá a mensagem por completo da fila quando esta tiver recebido uma notificação desta mensagem (ou grupo de mensagens).

Em certas situações, quando uma mensagem não puder ser roteada, ela poderá ser retornada para seu criador, descartada ou se o *broker8* (corretor) implementar uma extensão chamada de "*dead letter queue*", ou fila da mensagem morta, os produtores podem escolher como manipular situações como esta apenas selecionando alguns parâmetros.,

Abaixo a imagen mostra como é a arquitetura AMQP.

![Arquitetura geral do protocolo AMQP ](https://imageshack.com/i/pmzd70Jqj)

## RabbitMQ

RabbitMQ é um software de corretor de mensagens de código aberto (às vezes chamado middleware orientado a mensagens ) que implementa o *Advanced Message Queuing Protocol* (AMQP). O servidor RabbitMQ está escrito na linguagem de programação Erlang e é construído na plataforma *Open Telecom Platform* para *cluster* e *failover*. Bibliotecas de clientes para interagir com o *broker* (corretor) estão disponíveis para todas as principais linguagens de programação.

### Fluxo de mensagens em RabbitMQ com o mecanismo de Troca *Exchanges* (intercambiador)

![Arquitetura RabbitMQ](https://www.cloudamqp.com/img/blog/exchanges-bidings-routing-keys.png)

1- O *producer* (produtor) publica uma mensagem para uma troca. Quando você cria a troca, você tem que especificar o tipo dela.
Os diferentes tipos de *exchanges* (intercâmbios)) são explicados em detalhes mais adiante.

2- A central recebe a mensagem e agora é responsável pelo encaminhamento da mensagem.
A troca tem diferentes atributos de mensagem em conta, como a chave de roteamento, dependendo do tipo de troca.

3- As ligações têm de ser criadas a partir da troca para filas. Neste caso vemos duas ligações a duas filas diferentes da troca. O *Exchange* (intercambiador) encaminha a mensagem para as filas, dependendo dos atributos da mensagem.

4- As mensagens permanecem na fila até que sejam tratadas por um *subscribe* (consumidor). O subscribe processa a mensagem.

[Tutorial RabbitMQ para Iniciantes](https://www.cloudamqp.com/blog/2015-05-18-part1-rabbitmq-for-beginners-what-is-rabbitmq.html)

Para obter mais informações sobre RabbitMQ acesse o site oficial https://www.rabbitmq.com

## *Message Oriented Middleware* (Mensagem Orientado a Menssagem - MOM)

Middleware orientado a mensagem (MOM) é um software ou hardware de infraestrutura focada em enviar e receber mensagens entre sistemas distribuidos, que permite que os módulos do aplicativo para ser distribuido atraves de plataformas heterogeneas, também reduz a complexidade do desenvolvimento de aplicações que abrangem vários sistemas operacionais e protocolos de rede. MOM é um software que reside em ambas porções da arquitetura cliente/servidor e normalmente suporta chamadas assíncronasa entre as aplicações clientes e servidor.

Utilizando todos os conceitos a cima o figura a baixo mostra como é a arquitura do projeto 3.

![Arquitetura PROJETO 3](https://imageshack.com/i/pm3rThWlp)

Na figura acima temos os passos.

1 - Cliente web envia msg que fica armazena no cache no proxy;

2 - Registro de conexão (objeto msg {id, nick, canal...};

3 - Cria fila user-1 e escreve;

4 - Recebe mensagem pelo registro de conexão;

5 - Inscreve no user-1;

6 - Msg 2 do servidor IRC;

7 - Envio da msg 2 para user-1;

8 - Inseri msg 2 no cache;

9 - Cliente web lê a msg 2 via polling.

****observação
* melhorar o diagrama;
* criar o diagrama de sequencia
* explicar melhor cada passo da arquitetura e suas interações.


