var irc = require('irc');
var amqp = require('amqplib/callback_api');


var proxies = {}; // mapa de proxys
var amqp_conn;
var amqp_ch;
var irc_client;


// Conexão com o servidor AMQP - Criamos um servidor gratuito com o serviço do cloudamqp
amqp.connect('amqp://tsbfiaix:qv-OlaLg8uIubDP1bf--C1JwQTC67U2I@orangutan.rmq.cloudamqp.com/tsbfiaix', function(err, conn) {
	conn.createChannel(function(err, ch) {
		amqp_conn = conn;
		amqp_ch = ch;

		inicializar();
	});
});

function inicializar () {

	receberDoCliente("registro_conexao", function (msg) {

		var id       = msg.id;
		var servidor = msg.servidor;
		var nick     = msg.nick;
		var canal    = msg.canal;

		irc_client = new irc.Client(
			servidor,
			nick,
			{channels: [canal]}
		);
    irc_client.nicks = {};



    //ouve o evento mensagem e envia para o cliente irc
		irc_client.addListener('message'+canal, function (from, message) {
		    console.log(from + ' => '+ canal +': ' + message);
		    enviarParaCliente(id, {
		    	"timestamp": Date.now(),
				"nick": from,
				"msg": message
			});
		});

        irc_client.addListener('error', function(message) {
            console.log('error: ', message) });

    //ouve o evento motd e envia para o cliente a mensagem inicial de conexao com o servidor irc
        irc_client.addListener('motd', function(message) {
            console.log('motd: ', message);
            var motdMsg = message.replace(new RegExp('\r?\n','g'), '<br>'); //quebra de linha da mensagem motd
            enviarParaCliente(id, {
                "timestamp": Date.now(),
                "nick": nick,
                "msg": '<br>'+motdMsg
            });
        });

        //ouve o evento names e envia para o cliente lista de nicks
        irc_client.addListener('names', function(canal, nicks) {
            enviarParaCliente(id, {
                "timestamp": Date.now(),
                "nick": nick,
                "msg": 'nicks do canal: '+irc_client.nicks[canal]
            });
            console.log('names: ', irc_client.nicks[canal]);
        });
        //EVENTO DE KICKAR UM USUARIO
        irc_client.addListener('kick', function(channel, who, by, reason) {
            console.log('%s was kicked from %s by %s: %s', who, channel, by, reason);
        });
        //EVENTO DE ENTRAR NO CANAL
        irc_client.addListener('join', function(channel, who) {
            console.log('%s entrou no canal %s', who, channel);
            enviarParaCliente(id, {
                "timestamp": Date.now(),
                "nick": nick,
                "msg": '<font color = "Blue">'+ who+' entrou no canal '+ irc_client.opt.channels[0]+'</font>'
            });
            irc_client.say(who, 'Ola! seja bem vindo ao canal ' + channel);
        });
        //EVENTO DE REGISTERED
        irc_client.addListener('registered', function(message) {
            console.log('registered :'+JSON.stringify(message));
            enviarParaCliente(id, {
                "timestamp": Date.now(),
                "nick": nick,
                "msg": '<font color = "Green">' +'registrado no servidor : '+servidor+'</font><br>'
            });
        });


        irc_client.addListener('ctcp-privmsg', function (from, to, text, message) {
            console.log('ctcp-privmsg :'+JSON.stringify(message));

            enviarParaCliente(id, {
                "timestamp": Date.now(),
                "nick": nick,
                "msg": '<font color = "Red">'+message+'</font><br>'
            });

        });


        irc_client.addListener('nick', function (oldnick, newnick, channels, message) {
            console.log('nick :'+JSON.stringify(message));

            enviarParaCliente(id, {
                "timestamp": Date.now(),
                "nick": nick,
                "msg": '<font color = "Yellow">'+message+'</font><br>'
            });

        });


        irc_client.addListener('whois', function (message) {
            console.log('whois'+JSON.stringify(message));

            enviarParaCliente(id, {
                "timestamp": Date.now(),
                "nick": nick,
                "msg": '<font color = "Red">'+JSON.stringify(message)+'</font><br>'
            });

        });
        irc_client.addListener('raw', function(message) {
            console.log('mensagem recebida:'+JSON.stringify(message));
           /* enviarParaCliente(id, {
                "timestamp": Date.now(),
                "nick": nick,
                "msg": 'Raw: '+'<font color = "Blue">'+JSON.stringify(message)+'</font><br>'+'<font color = "Red">'+JSON.stringify(message)+'</font><br>'
            });*/
        });

// disparado quando iniciar a listagem de canal
        irc_client.addListener('channellist_start', function() {
            console.log('iniciei a listagem de canal');

        });

// disparado quando uma flag (mode) for atribuida
        irc_client.addListener('+mode', function(channel,by,mode,argument,message) {
            console.log('ocorreu alteração de mode:');
            enviarParaCliente(id, {
                "timestamp": Date.now(),
                "nick": nick,
                "msg": '+mode: '+'<font color = "Blue">'+'canal:'+channel+' by:'+by+' mode:'
                +mode+' argument:'+argument+' message:'+JSON.stringify(message)+'</font><br>'
            });
        });



        irc_client.nicks[canal] = nick;
        proxies[id] = irc_client;
	});

	receberDoCliente("gravar_mensagem", function (msg) {
        if (msg.msg.charAt(0) == '/'){
            var cmd = msg.msg.substring(1, 20).toLowerCase();
            switch (cmd) {
                case "motd": {
                    irc_client.send("motd");
                    break;
                }
                case "names": {
                    var user_canal = irc_client.opt.channels[0];
                    irc_client.send("names", user_canal);
                    break;
                }
                case "quit": {
                    irc_client.send("quit");
                    irc_client.disconnect();
                    enviarParaCliente(irc_client.id, '/quitOk');
                    delete proxies[irc_client.id];
                    delete irc_client;
                    break;
                }
                case "join":
                    {
                    if(irc_client.channels[cmd.split(" ")[1]])
                        irc_client.send("error");
                    else
                        {
                        irc_client.channels.push(cmd.split(" ")[1]);
                            irc_client.join(cmd.split(" ")[1], function(err) {
                                if (err) {
                                    console.log("Houve algo de errado!: " + err);
                                } else {
                                    console.log("Entrou no canal", cmd.split(" ")[1]);
                                }
                            });
                        }
                    break;
                    }
                case "privmsg":
                {
                    irc_client.ctcp(cmd.split(":")[1], 'privmsg',msg);
                    break;
                }
                case "whois":
                {

                    irc_client.send("whois",cmd.split(" ")[1]);
                    break;
                }
                case "nick":
                {
                irc_client.send('nick', message);
                break;
                }

                case "part":
                {
                    irc_client.part(irc_client.opt.channels[0]);

                    break;
                }




                default:
                    break;
                     }

        }

        console.log(msg);
		irc_client.say(msg.canal, msg.msg);
	});
}

function receberDoCliente (canal, callback) {

	amqp_ch.assertQueue(canal, {durable: false});
    console.log(" [irc] Waiting for messages in ", canal);
    amqp_ch.consume(canal, function(msg) {
      console.log(" [irc] Received %s", msg.content.toString());
      callback(JSON.parse(msg.content.toString()));
    }, {noAck: true});

}

function enviarParaCliente (id, msg) {

	msg = new Buffer(JSON.stringify(msg));
    amqp_ch.assertQueue("user_"+id, {durable: false});
    amqp_ch.sendToQueue("user_"+id, msg);
	console.log(" [irc] Sent %s", msg);
}
